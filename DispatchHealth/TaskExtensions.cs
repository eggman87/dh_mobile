﻿using System;
using System.Threading.Tasks;

namespace DispatchHealth
{
	public static class TaskExtensions
	{
		public static void Execute<TResult>(this Task<TResult> task, IView view, Action<TResult> uiCallback)
		{
			if (view == null)
			{
				return;
			}

			task.ContinueWith((result) =>
			{
				if (result.Exception != null)
				{
					//add error handling.
				    view?.ShowErrorMessage(result.Exception.Message);
				    return;
				}
				if (result.IsCompleted)
				{
				    view?.InvokeOnMainThread(() =>
				    {
				        uiCallback(result.Result);
				    });
				}
			});
		}
	}
}
