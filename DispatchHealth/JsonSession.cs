﻿using System;
using Newtonsoft.Json;
using StationApi;

namespace DispatchHealth
{
	public class JsonSession : ISession
	{
		private ITextStore store;
		private SessionState state;
		private bool needsToSave;

		private const string KEY_SESSION = "session"; 

		public JsonSession(ITextStore store)
		{
			this.store = store;
			LoadFromStore(); 
		}

		public User GetUser()
		{
			return state.user; 
		}

		public void SetUser(User user)
		{
			state.user = user;
			needsToSave = true;
		}

		private void LoadFromStore()
		{
		    var json = store.LoadText(KEY_SESSION);
		    state = json != null ? JsonConvert.DeserializeObject<SessionState>(json) : new SessionState();
		}

		public void WriteToStore()
		{
			var json = JsonConvert.SerializeObject(state);
			store.SaveText(KEY_SESSION, json); 
		}

		public void PersistIfNecessary()
		{
			if (needsToSave)
			{
				WriteToStore();
			}
		}
	}
}
