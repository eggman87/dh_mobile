﻿using StationApi;

namespace DispatchHealth
{
	public interface ISession
	{
		void SetUser(User user);

		User GetUser();

		void PersistIfNecessary(); 
	}
}
