﻿namespace DispatchHealth
{
	public interface ITextStore
	{
		void SaveText(string key, string text);

		string LoadText(string key); 
	}
}
