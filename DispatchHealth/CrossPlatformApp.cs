﻿using Autofac;
using StationApi;

namespace DispatchHealth
{
	public class CrossPlatformApp
	{
		public static IContainer Container { get; set; } 

		public static void Initalize(IViewBinder viewBinder)
		{
			var builder = new ContainerBuilder();

			builder.Register(c => new UserService()).As<IUserService>();

			viewBinder.BindViewLayer(builder);

			builder.Register(c => new JsonSession(c.Resolve<ITextStore>())).As<ISession>().SingleInstance();

			Container = builder.Build();
		}
	}
}
