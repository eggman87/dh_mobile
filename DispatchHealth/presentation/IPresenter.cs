﻿namespace DispatchHealth
{
	public interface IPresenter<TView> where TView : IView
	{
		void Attach(TView view);

		void Detach(); 
	}
}
