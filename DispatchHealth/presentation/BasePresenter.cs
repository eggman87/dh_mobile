﻿using System;
using System.Threading.Tasks;

namespace DispatchHealth
{
	public abstract class BasePresenter<TView>: IPresenter<TView> where TView : IView
	{
		protected TView View;

		public virtual void Attach(TView view)
		{
			View = view;
		}

		public virtual void Detach()
		{
			View = default(TView);
		}

		protected void OnUi(Action action)
		{
			if (View != null)
			{
				View.InvokeOnMainThread(action);
			}
		}

		protected void RunTask<TReturn>(Task<TReturn> bgFunc, Action<TReturn> uiCallback)
		{
			if (View == null)
			{
				return; 
			}

			bgFunc.ContinueWith((result) => {
				OnUi(() => {
					if (result.IsCompleted)
					{
						uiCallback(bgFunc.Result); 
					}
				});
			});
		}
	}
}
