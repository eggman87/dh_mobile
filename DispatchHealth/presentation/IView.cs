﻿using System;

namespace DispatchHealth
{
	public interface IView
	{
		void ShowErrorMessage(string message);

		void InvokeOnMainThread(Action action);
	}
}
