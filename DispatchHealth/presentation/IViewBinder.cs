﻿using Autofac;

namespace DispatchHealth
{
	public interface IViewBinder
	{
		void BindViewLayer(ContainerBuilder builder); 
	}
}
