﻿using StationApi;

namespace DispatchHealth
{
	public interface IHomeView : IView
	{
		void ShowUserInfo(User user); 
	}
}
