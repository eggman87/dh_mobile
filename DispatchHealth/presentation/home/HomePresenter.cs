﻿using Autofac;
using StationApi;

namespace DispatchHealth
{
	public class HomePresenter : BasePresenter<IHomeView>
	{
	    private readonly IUserService _userService;
	    private readonly ISession _session;

	    public HomePresenter()
	    {
	        _userService = CrossPlatformApp.Container.Resolve<IUserService>();
	        _session = CrossPlatformApp.Container.Resolve<ISession>();
	    }

		public void LoadHomeStuff()
		{
			var currentUser = _session.GetUser();
			if (currentUser != null)
			{
				View.ShowUserInfo(currentUser);
			}
			else
			{

				var creds = new UserCredentials("maharris.us@gmail.com", "sage1234");
				_userService.LoginUser(creds).Execute(View, user => OnUserLoaded(user));
			}
		}

		private void OnUserLoaded(User user)
		{
			_session.SetUser(user);

			View.ShowUserInfo(user);
		}
	}
}
