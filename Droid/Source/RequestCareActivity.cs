﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;

namespace DispatchHealth.Droid.Source
{
	[Activity(Label = "RequestCareActivity", Theme = "@style/AppTheme")]
	public class RequestCareActivity : AppCompatActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.activity_request_care);


			var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.act_request_care_tb_title);
			SetSupportActionBar(toolbar);

			SupportActionBar.Title = ""; 
			SupportActionBar.SetDisplayHomeAsUpEnabled(true);
			SupportActionBar.SetDisplayShowHomeEnabled(true);
			toolbar.NavigationClick += (sender, e) => {
				OnBackPressed(); 
			};
		}
	}
}
