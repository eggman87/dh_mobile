﻿using System;
using Android.Support.V7.App;
using Android.Widget;
using Autofac;

namespace DispatchHealth.Droid.Source
{
	public class BaseActivity<TPresenter, TView> : AppCompatActivity, IView 
		where TPresenter:IPresenter<TView> 
		where TView:IView
	{
		protected TPresenter Presenter;
		protected ISession Session;

		protected override void OnCreate(Android.OS.Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			Session = CrossPlatformApp.Container.Resolve<ISession>();

			Presenter = CreatePresenter();
			Presenter.Attach(GetView());
		}

		protected override void OnPause()
		{
			base.OnPause();

			Session.PersistIfNecessary();
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();

			Presenter.Detach();
		}

		public void InvokeOnMainThread(Action action)
		{
			RunOnUiThread(action); 
		}

		public void ShowErrorMessage(string message)
		{
			Toast.MakeText(this, message, ToastLength.Short).Show();
		}

		protected virtual TPresenter CreatePresenter() 
		{
			return default(TPresenter);
		}

		protected virtual TView GetView()
		{
			return default(TView); 
		}
	}
}
