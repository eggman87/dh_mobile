﻿using Android.Content;
using Android.Preferences;

namespace DispatchHealth.Droid.Source
{
	public class AndroidTextStore : ITextStore 
	{
		private readonly Context _context;

		public AndroidTextStore(Context context)
		{
			_context = context;
		}

		public string LoadText(string key)
		{
			var prefs = PreferenceManager.GetDefaultSharedPreferences(_context);
			return prefs.GetString(key, null); 
		}

		public void SaveText(string key, string text)
		{
			var prefs = PreferenceManager.GetDefaultSharedPreferences(_context);
			prefs.Edit().PutString(key, text).Apply(); 
		}
	}
}
