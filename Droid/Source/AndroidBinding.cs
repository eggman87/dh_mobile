﻿using Android.Content;
using Autofac;

namespace DispatchHealth.Droid.Source
{
	public class AndroidBinding : IViewBinder
	{
		private readonly Context _context;

		public AndroidBinding(Context context)
		{
			_context = context;
		}

		public void BindViewLayer(ContainerBuilder builder)
		{
			builder.Register(c => new AndroidTextStore(_context)).As<ITextStore>();
		}
	}
}
