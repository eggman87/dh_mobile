﻿using System;
using Android.App;
using Android.Runtime;

namespace DispatchHealth.Droid.Source
{
	[Application]
	public class DispatchApplication : Application 
	{
		public DispatchApplication(IntPtr handle, JniHandleOwnership ownerShip) : base(handle, ownerShip)
    	{
			
		}

		public override void OnCreate()
		{
			base.OnCreate();

			CrossPlatformApp.Initalize(new AndroidBinding(this)); 
		}
	}
}
