﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Util;
using Android.Widget;
using StationApi;

namespace DispatchHealth.Droid.Source
{
	[Activity(Label = "DispatchHealth", MainLauncher = true, Icon = "@mipmap/icon", Theme = "@style/AppTheme")]
	public class MainActivity : BaseActivity<HomePresenter, IHomeView>, IHomeView
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			StrictMode.SetThreadPolicy(new StrictMode.ThreadPolicy.Builder().DetectDiskReads().DetectDiskWrites().DetectNetwork().PenaltyLog().Build());

			Presenter.LoadHomeStuff();

			SetContentView(Resource.Layout.activity_home);

			var btnWhatsWrong = FindViewById<TextView>(Resource.Id.act_home_tv_whats_wrong);
			btnWhatsWrong.Click += (sender, e) => {
				GoToRequestCare();
			};
		}

		public void ShowUserInfo(User user)
		{
			ShowErrorMessage("Loaded: " + user.FirstName + " " + user.LastName);
		}

		protected override HomePresenter CreatePresenter()
		{
			return new HomePresenter(); 
		}

		protected override IHomeView GetView()
		{
			return this; 
		}

		private void GoToRequestCare()
		{
			var intent = new Intent(this, typeof(RequestCareActivity));
			var sharedElements = new Pair[3];
			sharedElements[0] = new Pair(FindViewById(Resource.Id.act_home_blue_container), "blue_container");
			sharedElements[1] = new Pair(FindViewById(Resource.Id.act_home_white_container), "white_container");
			sharedElements[2] = new Pair(FindViewById(Resource.Id.act_home_iv_logo), "logo");

			var options = ActivityOptionsCompat.MakeSceneTransitionAnimation(this, sharedElements);
			StartActivity(intent, options.ToBundle()); 
		}
	}
}

