﻿using System;
using UIKit;

namespace DispatchHealth.iOS
{
	public static class UIViewExtensions
	{
		public static UIView Clone(this UIView self)
		{
			UIView clone;

			if (self is UIImageView)
			{
				var image = new UIImageView(frame: self.Frame);
				image.Image = ((UIImageView)self).Image;

				clone = image;
			}
			else if (self is UILabel)
			{
				var lbl = new UILabel(frame: self.Frame);
				var selfAsLbl = (UILabel)self;

				lbl.Text = selfAsLbl.Text;
				lbl.Font = selfAsLbl.Font;
				lbl.TextColor = selfAsLbl.TextColor;

				clone = lbl; 
			}
			else
			{
				clone = new UIView(frame: self.Frame); 
			}

			clone.Layer.CornerRadius = self.Layer.CornerRadius;
			clone.BackgroundColor = self.BackgroundColor;
			clone.Opaque = self.Opaque;
			clone.Layer.MasksToBounds = self.Layer.MasksToBounds;
			clone.Layer.ShadowColor = self.Layer.ShadowColor;
			clone.Layer.ShadowOffset = self.Layer.ShadowOffset; 

			return clone; 
		}
	}
}