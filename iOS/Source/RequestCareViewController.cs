﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;

namespace DispatchHealth.iOS
{
	public partial class RequestCareViewController : UIViewController, ITransitionElementProvider
	{
		public RequestCareViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}

		//I think eventually we will have to pass one of these for every VC that has transitions to a future VC
		//[Export("navigationController:animationControllerForOperation:fromViewController:toViewController:")]
		//public IUIViewControllerAnimatedTransitioning GetAnimationControllerForOperation(UINavigationController navigationController, UINavigationControllerOperation operation, UIViewController fromViewController, UIViewController toViewController)
		//{
		//	return new ElementTransitionAnimationController();
		//}

		public void ToTransition(ElementTransitionAnimationController controller)
		{
			controller.SetToFrameElements(this.GetTransitionElements());
			controller.SetFromAlphaElements(new List<UIView> { this.LblDisclaimer });
		}

		public void FromTransition(ElementTransitionAnimationController controller)
		{
			controller.SetFromFrameElements(this.GetTransitionElements());
			controller.SetFromAlphaElements(new List<UIView> { this.LblDisclaimer }); 
		}

		private Dictionary<string, UIView> GetTransitionElements()
		{
			return new Dictionary<string, UIView>()
			{
				{ "ContainerBlue", this.ContainerBlue },
				{ "ContainerWhatsWrong", this.ContainerWhatsWrong },
				{ "ImgLogo", ImgLogo }
			};
		}
	}
}

