﻿using System;
using Foundation;

namespace DispatchHealth.iOS
{
	public class AppleTextStore : ITextStore
	{
		public string LoadText(string key)
		{
			var defaults = NSUserDefaults.StandardUserDefaults;
			return defaults.StringForKey(key); 
		}

		public void SaveText(string key, string text)
		{
			var defaults = NSUserDefaults.StandardUserDefaults;
			defaults.SetString(text, key);
			defaults.Synchronize(); 
		}
	}
}
