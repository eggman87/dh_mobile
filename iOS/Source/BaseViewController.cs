﻿using System;
using UIKit;

namespace DispatchHealth.iOS
{
	public abstract class BaseViewController<TPresenter, TView>: UIViewController, IView
		where TPresenter:IPresenter<TView>
		where TView:IView
	{

		protected TPresenter presenter; 

		public BaseViewController(IntPtr handle) : base(handle)
		{
			
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			presenter = GetPresenter();
			presenter.Attach(GetView()); 
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);

			if (IsBeingDismissed)
			{
				presenter.Detach();
			}
		}

		public void ShowErrorMessage(string message)
		{
			UIAlertController alertController = UIAlertController.Create("Hey There", message, UIAlertControllerStyle.Alert);
			alertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, alert => alertController.DismissViewController(false, null)));
			PresentViewController(alertController, true, null);
		}

		protected virtual TView GetView()
		{
			return default(TView); 
		}

		protected virtual TPresenter GetPresenter()
		{
			return default(TPresenter); 
		}
	}
}
