﻿using System;
using Autofac;

namespace DispatchHealth.iOS
{
	public class AppleBinding : IViewBinder
	{
		public void BindViewLayer(ContainerBuilder builder)
		{
			builder.Register(c => new AppleTextStore()).As<ITextStore>();
		}
	}
}
