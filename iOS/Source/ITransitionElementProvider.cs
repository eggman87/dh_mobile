﻿using System;
namespace DispatchHealth.iOS
{
	public interface ITransitionElementProvider
	{
		void ToTransition(ElementTransitionAnimationController controller);
		void FromTransition(ElementTransitionAnimationController controller); 
	}
}
