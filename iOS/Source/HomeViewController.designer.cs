// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace DispatchHealth.iOS
{
    [Register ("HomeViewController")]
    partial class HomeViewController
    {
        [Outlet]
        UIKit.UIButton Button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnWhatsWrong { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ContainerBlue { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ContainerWhatsWrong { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView ImgLogo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LblDisclaimer { get; set; }

        [Action ("ButtonWhatsWrongTapped:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void ButtonWhatsWrongTapped (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (btnWhatsWrong != null) {
                btnWhatsWrong.Dispose ();
                btnWhatsWrong = null;
            }

            if (ContainerBlue != null) {
                ContainerBlue.Dispose ();
                ContainerBlue = null;
            }

            if (ContainerWhatsWrong != null) {
                ContainerWhatsWrong.Dispose ();
                ContainerWhatsWrong = null;
            }

            if (ImgLogo != null) {
                ImgLogo.Dispose ();
                ImgLogo = null;
            }

            if (LblDisclaimer != null) {
                LblDisclaimer.Dispose ();
                LblDisclaimer = null;
            }
        }
    }
}