﻿using System;
using System.Collections.Generic;
using System.Threading;
using Foundation;
using StationApi;
using UIKit;

namespace DispatchHealth.iOS
{
	public partial class HomeViewController : BaseViewController<HomePresenter, IHomeView>, IHomeView, 
				IUIViewControllerTransitioningDelegate, IUINavigationControllerDelegate, ITransitionElementProvider
	{
		private ElementTransitionAnimationController animationcontroller = new ElementTransitionAnimationController(); 

		public HomeViewController(IntPtr handle) : base(handle)
		{
			this.NavigationController.Delegate = this; 
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			Console.WriteLine("ViewDidAppear=" + NSThread.Current.IsMainThread); 
			presenter.LoadHomeStuff();
		}

		public void ShowUserInfo(User user)
		{
			ShowErrorMessage("Loaded: " + user.FirstName + " " + user.LastName); 
		}

		protected override HomePresenter GetPresenter()
		{
			return new HomePresenter(); 
		}

		protected override IHomeView GetView()
		{
			return this; 
		}

		partial void ButtonWhatsWrongTapped(UIButton sender)
		{
			PerformSegue("requestCareFromHome", this); 
		}

		[Export("navigationController:animationControllerForOperation:fromViewController:toViewController:")]
		public IUIViewControllerAnimatedTransitioning GetAnimationControllerForOperation(UINavigationController navigationController, UINavigationControllerOperation operation, UIViewController fromViewController, UIViewController toViewController)
		{
			return animationcontroller;  
		}

		public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
		{
			base.PrepareForSegue(segue, sender);

			segue.DestinationViewController.TransitioningDelegate = this; 
		}

		[Action("UnwindToHome:")]
		public void UnwindToHome(UIStoryboardSegue segue)
		{
		}

		public void ToTransition(ElementTransitionAnimationController controller)
		{
			controller.SetToFrameElements(this.GetTransitionElements());
			controller.SetToAlphaElements(new List<UIView> { LblDisclaimer}); 
		}

		public void FromTransition(ElementTransitionAnimationController controller)
		{
			controller.SetFromFrameElements(this.GetTransitionElements());
			controller.SetFromAlphaElements(new List<UIView> { LblDisclaimer}); 
		}

		private Dictionary<string, UIView> GetTransitionElements()
		{
			return new Dictionary<string, UIView>()
			{
				{ "ContainerBlue", this.ContainerBlue },
				{ "ContainerWhatsWrong", this.ContainerWhatsWrong },
				{ "ImgLogo", this.ImgLogo }
			};
		}
	}
}
