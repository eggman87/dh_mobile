// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace DispatchHealth.iOS
{
    [Register ("RequestCareViewController")]
    partial class RequestCareViewController
    {
        [Outlet]
        UIKit.UIImageView ImgLogo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton BtnBack { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ContainerBlue { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView ContainerWhatsWrong { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LblDisclaimer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (BtnBack != null) {
                BtnBack.Dispose ();
                BtnBack = null;
            }

            if (ContainerBlue != null) {
                ContainerBlue.Dispose ();
                ContainerBlue = null;
            }

            if (ContainerWhatsWrong != null) {
                ContainerWhatsWrong.Dispose ();
                ContainerWhatsWrong = null;
            }

            if (ImgLogo != null) {
                ImgLogo.Dispose ();
                ImgLogo = null;
            }

            if (LblDisclaimer != null) {
                LblDisclaimer.Dispose ();
                LblDisclaimer = null;
            }
        }
    }
}