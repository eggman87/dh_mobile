﻿using System;
using System.Collections.Generic;
using UIKit;

namespace DispatchHealth.iOS
{
	/// <summary>
	/// Element transition animation controller.
	/// 
	/// Responsible for providing material like transition animations between two view controllers, where elements (views) 
	/// can be mapped from VC A to elements in VC B. There are two types of two element mappings. Alpha mapping 
	/// where elements will use fade in/out but stay in the same frame. The other type is just frame to frame animation 
	/// where the elements will animate from current frame to the mapping objects frame. 
	/// 
	/// Alpha animations work best for thing like textviews/labels. 
	/// Frame animations work best for things like images or containers that change size from VC a to b. 
	/// 
	/// </summary>
	public class ElementTransitionAnimationController : UIViewControllerAnimatedTransitioning
	{
		Dictionary<string, UIView> toElements;
		Dictionary<string, UIView> fromElements;

		List<UIView> toFadeInElements;
		List<UIView> fromFadeInElements;

		private const double ANIMATION_DURATION = 0.5;

		public override void AnimateTransition(IUIViewControllerContextTransitioning transitionContext)
		{
			var containerView = transitionContext.ContainerView;
			var fromVc = transitionContext.GetViewControllerForKey(UITransitionContext.FromViewControllerKey);
			var toVc = transitionContext.GetViewControllerForKey(UITransitionContext.ToViewControllerKey);

			toVc.View.SetNeedsLayout();
			toVc.View.LayoutIfNeeded();

			ITransitionElementProvider fromProvider = fromVc as ITransitionElementProvider;
			ITransitionElementProvider toProvider = toVc as ITransitionElementProvider;

			if (toProvider == null)
			{
				transitionContext.CompleteTransition(true); 
				return; 
			}

			fromProvider.FromTransition(this);
			toProvider.ToTransition(this); 

			toVc.View.Frame = transitionContext.GetFinalFrameForViewController(toVc);
			containerView.AddSubview(toVc.View);
			toVc.View.Alpha = 0.0f;

			foreach (var element in this.fromFadeInElements)
			{
				element.Alpha = 0.0f;
			}

			var snapshots = new Dictionary<string, UIView>();
			foreach (var item in this.fromElements)
			{
				var snapshot = item.Value.Clone();
				containerView.AddSubview(snapshot);
				snapshots.Add(item.Key, snapshot); 

				this.toElements[item.Key].Alpha = 0.0f;
			}

			var duration = this.TransitionDuration(transitionContext);

			UIView.Animate(duration, () =>
			{
				//animations
				toVc.View.Alpha = 1.0f;
				fromVc.View.Alpha = 0.0f;

				foreach (var item in snapshots)
				{
					var toElement = this.toElements[item.Key];
					if (toElement != null)
					{
						item.Value.Frame = toElement.Frame;
					}
				}

			}, () =>
			{
				//completion 
				foreach (var item in this.toElements)
				{
					item.Value.Alpha = 1.0f;
				}

				foreach (var item in snapshots)
				{
					item.Value.RemoveFromSuperview();
				}

				foreach (var item in this.fromFadeInElements)
				{
					item.Alpha = 1.0f;
				}
				transitionContext.CompleteTransition(true);
			});

		}

		private void AnimateFadeInToElements()
		{
			foreach (var item in this.toFadeInElements)
			{
				item.Alpha = 0.0f;
			}

			UIView.Animate(ANIMATION_DURATION, () =>
			{
				foreach (var item in this.toFadeInElements)
				{
					item.Alpha = 1.0f;
				}
			});
		}

		public override double TransitionDuration(IUIViewControllerContextTransitioning transitionContext)
		{
			return ANIMATION_DURATION;
		}

		public void SetToFrameElements(Dictionary<string, UIView> elements)
		{
			toElements = elements;
		}

		public void SetFromFrameElements(Dictionary<string, UIView> elements)
		{
			fromElements = elements;
		}

		public void SetToAlphaElements(List<UIView> elements)
		{
			toFadeInElements = elements;
		}

		public void SetFromAlphaElements(List<UIView> elements)
		{
			fromFadeInElements = elements;
		}
	}
}
