﻿using System;
namespace StationApi
{
	public class UserCredentials
	{
		public string email;
		public string password; 

		public UserCredentials(string email, string password)
		{
			this.email = email;
			this.password = password; 
		}
	}
}
