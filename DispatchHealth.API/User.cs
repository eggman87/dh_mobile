﻿using System;
namespace StationApi
{
	public class User
	{
		public string Email { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }

	}
}
