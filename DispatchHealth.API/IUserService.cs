﻿using System;
using System.Threading.Tasks;

namespace StationApi
{
	public interface IUserService
	{
		Task<User> LoginUser(UserCredentials credentials);
	}
}
