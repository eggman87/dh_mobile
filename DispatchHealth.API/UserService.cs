﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
//using Foundation;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace StationApi
{
	public class UserService : IUserService
	{
		private HttpClient client; 

		public const string LOGIN_URL = "https://staging.dispatchhealth.com/users/sign_in.json";

		public UserService()
		{
			client = new HttpClient(); 
		}

		public async Task<User> LoginUser(UserCredentials credentials)
		{
			throw new ArgumentOutOfRangeException("bro"); 
			//Console.WriteLine("loginUser=" + NSThread.Current.IsMainThread);
			var uri = new Uri(LOGIN_URL);
			var resolver = new DefaultContractResolver();
			resolver.NamingStrategy = new SnakeCaseNamingStrategy(); 
			var jsonSettings = new JsonSerializerSettings
			{
				ContractResolver = resolver, 
				NullValueHandling = NullValueHandling.Ignore
			};

			var json = JsonConvert.SerializeObject(new LoginUserWrapper(credentials));

			var payload = new StringContent(json, Encoding.UTF8, "application/json"); 
			var response = await client.PostAsync(uri, payload);

			if (response.IsSuccessStatusCode)
			{
				var content = await response.Content.ReadAsStringAsync();
				var user = JsonConvert.DeserializeObject<User>(content, jsonSettings);
				return user; 
			}
			return null;
		}
	}
}
